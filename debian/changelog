r-cran-readr (2.1.3-1) unstable; urgency=medium

  * New upstream release

  * debian/control: Set Build-Depends: to recent R version
  * debian/control: Set Standards-Version: to current version

 -- Dirk Eddelbuettel <edd@debian.org>  Sat, 01 Oct 2022 09:09:44 -0500

r-cran-readr (2.1.2-1) unstable; urgency=medium

  * New upstream release

 -- Dirk Eddelbuettel <edd@debian.org>  Sun, 30 Jan 2022 17:36:32 -0600

r-cran-readr (2.1.1-1) unstable; urgency=medium

  * New upstream release

 -- Dirk Eddelbuettel <edd@debian.org>  Fri, 03 Dec 2021 08:01:05 -0600

r-cran-readr (2.1.0-1) unstable; urgency=medium

  * New upstream release

  * debian/control: Set Build-Depends: to recent R version

 -- Dirk Eddelbuettel <edd@debian.org>  Fri, 12 Nov 2021 08:03:42 -0600

r-cran-readr (2.0.2-1) unstable; urgency=medium

  * New upstream release

  * debian/control: Set Build-Depends: to recent R version
  * debian/control: Set Standards-Version: to current version

 -- Dirk Eddelbuettel <edd@debian.org>  Mon, 11 Oct 2021 10:48:14 -0500

r-cran-readr (2.0.1-1) unstable; urgency=medium

  * New upstream release

 -- Dirk Eddelbuettel <edd@debian.org>  Sun, 10 Oct 2021 22:41:33 -0500

r-cran-readr (2.0.0-1) unstable; urgency=medium

  * New upstream release (belatedly in unstable as [build-dependency of
    vroom] r-cran-tzdb and r-cran-vroom had to be added to Debian)

  * debian/control: Set Build-Depends: to recent R version
  * debian/control: Set Standards-Version: to current version 

 -- Dirk Eddelbuettel <edd@debian.org>  Sun, 10 Oct 2021 12:26:48 -0500

r-cran-readr (1.4.0-1) unstable; urgency=medium

  * New upstream release

  * debian/control: Set Build-Depends: to current R version
  * debian/control: Switch to virtual debhelper-compat (= 11)
  * debian/compat: Removed
  * debian/control: Add new Build-Depends: on r-cran-cpp11

 -- Dirk Eddelbuettel <edd@debian.org>  Thu, 08 Oct 2020 17:55:14 -0500

r-cran-readr (1.3.1-1) unstable; urgency=medium

  * New upstream release
  
  * debian/control: Set Build-Depends: to current R version 

 -- Dirk Eddelbuettel <edd@debian.org>  Tue, 25 Dec 2018 15:33:28 -0600

r-cran-readr (1.3.0-1) unstable; urgency=medium

  * New upstream release

 -- Dirk Eddelbuettel <edd@debian.org>  Wed, 12 Dec 2018 20:26:36 -0600

r-cran-readr (1.2.1-2) unstable; urgency=medium

  * src/Makevars: Build without rcon to avoid rpath dance (Closes: #914633)

 -- Dirk Eddelbuettel <edd@debian.org>  Sun, 25 Nov 2018 15:08:17 -0600

r-cran-readr (1.2.1-1) unstable; urgency=medium

  * New upstream release
  
  * debian/control: Set Standards-Version: to current version 
  * debian/control: Set Build-Depends: to current R version 
  * debian/control: Add Vcs-Browser: and Vcs-Git:
  * debian/control: Switch from cdbs to dh-r
  * debian/rules: Idem

  * debian/control: Now use r-cran-bh which has since been added
  * debian/control: Add new (Build-)Depends r-cran-clipr

 -- Dirk Eddelbuettel <edd@debian.org>  Sat, 24 Nov 2018 17:48:21 -0600

r-cran-readr (1.1.1-1) unstable; urgency=low

  * Initial Debian release 				(Closes: #878675)

  * CRAN package BH used only at package-compile time removed from
    LinkingTo: in DESCRIPTION and replaced by libboost-all-dev

 -- Dirk Eddelbuettel <edd@debian.org>  Sun, 15 Oct 2017 13:02:55 -0500
